export class TodosDto {
  item: string;
  completed: boolean;
}