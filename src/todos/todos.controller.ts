import { Body, Controller, Get, Logger, Param, Post, Res } from "@nestjs/common";
import { TodosDto } from "./todos.dto";
import { TodosService } from "./todos.service";
import { Response } from "express";

@Controller("todos")
export class TodosController {

  constructor(private readonly todosService: TodosService) {
  }

  @Get()
  async findAll(@Res() response: Response) {
    return this.todosService.viewAll(response);

  }

  /* TODO fix logic in service before activating this
  @Post("size/:size")
  setSizeOfTodoList(@Param() size: number, @Res() response: Response): Response {
    Logger.log(size);
    return this.todosService.setSize(+size, response);
  }
   */

  @Get("getSize")
  getSizeOfTodoList(@Res() response: Response): Response {
    return this.todosService.getSize(response);
  }

  @Post("add")
  createNewTodoItem(@Body() request: TodosDto, @Res() response: Response): Response {
    Logger.log(request);
    return this.todosService.addToList(request, response);
  }

  @Post("remove/:itemIndex")
  removeTodoItem(@Param("itemIndex") itemIndex: number, @Res() response: Response): Response {
    Logger.log(itemIndex);
    return this.todosService.removeFromList(+itemIndex, response);
  }

  @Post("ackNack/:itemIndex")
  ackNackTodoItem(@Param("itemIndex") itemIndex: number, @Res() response: Response): Response {
    Logger.log(itemIndex);
    return this.todosService.ackNackTodoItemInList(+itemIndex, response);
  }

}
