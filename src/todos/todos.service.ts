import { HttpStatus, Injectable, Logger, Res } from "@nestjs/common";
import { TodosDto } from "./todos.dto";
import { Response } from "express";
import { JsonResponse, Status } from "./response.dto";

@Injectable()
export class TodosService {
  listSize: number; // size = 0 -> unlimited
  counter: number;
  currentList = new Map<number, TodosDto>();

  constructor() {
    this.listSize = 0;
    this.counter = 0;
  }

  viewAll(res: Response) {
    // this.response = response;
    const result = Object.fromEntries(this.currentList);
    Logger.log(result);
    return res
      .status(HttpStatus.OK)
      .json(new JsonResponse(Status.SUCCESS, '"List of all todos:" + JSON.stringify(result)'));
  }

  addToList(todoItem: TodosDto, response: Response): Response {
    this.counter++;
    Logger.log("Test: " + this.listSize);
    ;
    if (this.listSize > 0) {
      if (this.currentList.size < this.listSize) {
        this.currentList.set(this.counter, todoItem);
        Logger.log(this.currentList);
        return response
          .status(HttpStatus.CREATED)
          .json(new JsonResponse(Status.SUCCESS, 'ATodo item successfully added'));
      } else {
        Logger.log(this.currentList.size + " " + this.listSize + "List is full");
        return response
          .status(HttpStatus.BAD_REQUEST)
          .json(new JsonResponse(Status.FAILURE, 'List is full'));
      }
    } else {
      this.currentList.set(this.counter, todoItem);
      Logger.log(this.currentList);
      return response
        .status(HttpStatus.CREATED)
        .json(new JsonResponse(Status.SUCCESS, 'BTodo item successfully added'));
    }
  }

  removeFromList(itemNumber: number, response: Response): Response {
    Logger.log("Removal Index:" + itemNumber);
    if (this.currentList.size == 0) {
      Logger.log("List is empty");
      return response
        .status(HttpStatus.BAD_REQUEST)
        .json(new JsonResponse(Status.FAILURE, 'List is empty'));
    } else if (this.currentList.delete(itemNumber)) {
      return response
        .status(HttpStatus.CREATED)
        .json(new JsonResponse(Status.SUCCESS, 'Item successfully deleted'));
    } else {
      return response
        .status(HttpStatus.BAD_REQUEST)
        .json(new JsonResponse(Status.FAILURE, 'Delete failed'));
    }
  }

  ackNackTodoItemInList(itemNumber: number, response: Response): Response {
    Logger.log("Alter Index:" + itemNumber);
    let currentTodo = this.currentList.get(itemNumber);
    if (currentTodo != null) {
      Logger.log(currentTodo);
      currentTodo.completed = !currentTodo.completed;
      this.currentList.set(itemNumber, currentTodo);
      return response
        .status(HttpStatus.CREATED)
        .json(new JsonResponse(Status.SUCCESS, 'Item altered'));
    } else {
      Logger.log("Item not found");
      return response
        .status(HttpStatus.BAD_REQUEST)
        .json(new JsonResponse(Status.FAILURE, 'Item not found'));
    }

  }

  //TODO check this: getting NaN everytime we update
  setSize(size: number, response: Response): Response {
    this.listSize = size;
    Logger.log(typeof this.listSize);
    return response
      .status(HttpStatus.OK)
      .json(new JsonResponse(Status.SUCCESS, 'SUCCESS'));

  }

  getSize(response: Response): Response {
    return response
      .status(HttpStatus.OK)
      .json(new JsonResponse(Status.SUCCESS, "Size of List: " + this.listSize));
  }
}


