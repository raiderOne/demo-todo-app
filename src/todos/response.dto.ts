export enum Status {
  SUCCESS = "SUCCESS",
  FAILURE = "FAILURE"
}

export class JsonResponse {
  action: Status;
  message: any;


  constructor(action: Status, message: any) {
    this.action = action;
    this.message = message;
  }
}